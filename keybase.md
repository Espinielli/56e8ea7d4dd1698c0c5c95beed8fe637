### Keybase proof

I hereby claim:

  * I am espinielli on github.
  * I am espin (https://keybase.io/espin) on keybase.
  * I have a public key ASD-y-Lnlcg34rrlbt7wyX49NX5wC7kBit6ndj_b3Nrsugo

To claim this, I am signing this object:

```json
{
    "body": {
        "key": {
            "eldest_kid": "0120fecbe2e795c837e2bae56edef0c97e3d357e700bb9018adea7763fdbdcdaecba0a",
            "host": "keybase.io",
            "kid": "0120fecbe2e795c837e2bae56edef0c97e3d357e700bb9018adea7763fdbdcdaecba0a",
            "uid": "82082d61bed2bdf65ce59ae957011719",
            "username": "espin"
        },
        "service": {
            "name": "github",
            "username": "espinielli"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "client": {
        "name": "keybase.io go client",
        "version": "1.0.20"
    },
    "ctime": 1489166704,
    "expire_in": 504576000,
    "merkle_root": {
        "ctime": 1489166688,
        "hash": "3cb803137b3011dd921b35545ec55004248d8cd0794e9333d67d33edfca757a092bac98d3775e213a860d120c2ddbdfd659c627248a2857c7a9c1ab134daceb7",
        "seqno": 951179
    },
    "prev": "2cd01f84207e510fb816b5a88fd1ec91371758687e54f2b0ab2aa8d93d571a7c",
    "seqno": 6,
    "tag": "signature"
}
```

with the key [ASD-y-Lnlcg34rrlbt7wyX49NX5wC7kBit6ndj_b3Nrsugo](https://keybase.io/espin), yielding the signature:

```
hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEg/svi55XIN+K65W7e8Ml+PTV+cAu5AYrep3Y/29za7LoKp3BheWxvYWTFAut7ImJvZHkiOnsia2V5Ijp7ImVsZGVzdF9raWQiOiIwMTIwZmVjYmUyZTc5NWM4MzdlMmJhZTU2ZWRlZjBjOTdlM2QzNTdlNzAwYmI5MDE4YWRlYTc3NjNmZGJkY2RhZWNiYTBhIiwiaG9zdCI6ImtleWJhc2UuaW8iLCJraWQiOiIwMTIwZmVjYmUyZTc5NWM4MzdlMmJhZTU2ZWRlZjBjOTdlM2QzNTdlNzAwYmI5MDE4YWRlYTc3NjNmZGJkY2RhZWNiYTBhIiwidWlkIjoiODIwODJkNjFiZWQyYmRmNjVjZTU5YWU5NTcwMTE3MTkiLCJ1c2VybmFtZSI6ImVzcGluIn0sInNlcnZpY2UiOnsibmFtZSI6ImdpdGh1YiIsInVzZXJuYW1lIjoiZXNwaW5pZWxsaSJ9LCJ0eXBlIjoid2ViX3NlcnZpY2VfYmluZGluZyIsInZlcnNpb24iOjF9LCJjbGllbnQiOnsibmFtZSI6ImtleWJhc2UuaW8gZ28gY2xpZW50IiwidmVyc2lvbiI6IjEuMC4yMCJ9LCJjdGltZSI6MTQ4OTE2NjcwNCwiZXhwaXJlX2luIjo1MDQ1NzYwMDAsIm1lcmtsZV9yb290Ijp7ImN0aW1lIjoxNDg5MTY2Njg4LCJoYXNoIjoiM2NiODAzMTM3YjMwMTFkZDkyMWIzNTU0NWVjNTUwMDQyNDhkOGNkMDc5NGU5MzMzZDY3ZDMzZWRmY2E3NTdhMDkyYmFjOThkMzc3NWUyMTNhODYwZDEyMGMyZGRiZGZkNjU5YzYyNzI0OGEyODU3YzdhOWMxYWIxMzRkYWNlYjciLCJzZXFubyI6OTUxMTc5fSwicHJldiI6IjJjZDAxZjg0MjA3ZTUxMGZiODE2YjVhODhmZDFlYzkxMzcxNzU4Njg3ZTU0ZjJiMGFiMmFhOGQ5M2Q1NzFhN2MiLCJzZXFubyI6NiwidGFnIjoic2lnbmF0dXJlIn2jc2lnxECFYAsrfK5qJfHN+gCVMYy/tQEpo3qEWTTjwGNtTINZEoVCxDTrIMaVGNcJ8UtrKmQuk5e6jYJ6QmJmnufU/pQEqHNpZ190eXBlIKRoYXNogqR0eXBlCKV2YWx1ZcQgyn8WuOYIfnwRoab5C6SOQqRh/M+B7E9Ha3/HgenZVr+jdGFnzQICp3ZlcnNpb24B

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/espin

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id espin
```
